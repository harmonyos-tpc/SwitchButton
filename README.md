﻿# SwitchButton
SwitchButton is An beautiful+lightweight+custom-style-easy switch widget

#效果图
![SwitchButton](https://gitee.com/openharmony-tpc/SwitchButton/raw/master/SwitchButton.gif)

## 集成引用
方法一：
1、添加har包到libs文件夹内
2、在entry的gradle内添加如下代码：
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
}

方法二：
1、配置中心仓：
repositories {
    mavenCentral()
}
2、在entry的gradle内添加如下代码：
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:SwitchButton:1.0.2'
}

## entry运行要求
   通过DevEco studio,并下载HarmonySDK
   将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）



## 示例

```xml
    <com.suke.widget.SwitchButton
            ohos:left_margin="20vp"
            ohos:width="200vp"
            ohos:height="60vp"
            ohos:id="$+id:btn_switch1"
            />
 ```

```java
     SwitchButton switchButton1 = (SwitchButton) findComponentById(ResourceTable.Id_btn_switch1);
            switchButton1.setEnableEffect(true);
            switchButton1.isChecked();
            switchButton1.setShadowEffect(true);
            switchButton1.setShadowColor(0x4d888888);
            switchButton1.setShadowRadius(6);
            switchButton1.setChecked(true);
            switchButton1.setUncheckColor(0xcc888888);
            switchButton1.setBorderWidth(3);
            switchButton1.setEffectDuration(100);
            switchButton1.setShowIndicator(false);
            switchButton1.setOnCheckedChangeListener((view, isChecked) -> {

        });

   More Style:

    /**
     * 背景底色
     */
    private int background;
    /**
     * 背景关闭颜色
     */
    private int uncheckColor;
    /**
     * 背景打开颜色
     */
    private int checkedColor;
    /**
     * 边框宽度px
     */
    private int borderWidth;

    /**
     * 打开指示线颜色
     */
    private int checkLineColor;
    /**
     * 打开指示线宽
     */
    private int checkLineWidth;
    /**
     * 打开指示线长
     */
    private float checkLineLength;
    /**
     * 关闭圆圈颜色
     */
    private int uncheckCircleColor;
    /**
     * 关闭圆圈线宽
     */
    private int uncheckCircleWidth;
    /**
     * 关闭圆圈位移X
     */
    private float uncheckCircleOffsetX;
    /**
     * 关闭圆圈半径
     */
    private float uncheckCircleRadius;
    /**
     * 打开指示线位移X
     */
    private float checkedLineOffsetX;
    /**
     * 打开指示线位移Y
     */
    private float checkedLineOffsetY;
    /**
     * Color for button when it's uncheck
     */
    private int uncheckButtonColor;
    /**
     * Color for button when it's check
     */
    private int checkedButtonColor;

    /**
     * 是否选中
     */
    private boolean isChecked;
    /**
     * 是否启用动画
     */
    private boolean enableEffect;
    /**
     * 是否启用阴影效果
     */
    private boolean shadowEffect;
    /**
     * 是否显示指示器
     */
    private boolean showIndicator = true;

    /**
     * 阴影半径
     */
    private int shadowRadius;

    /**
     * 阴影颜色
     */
    private int shadowColor;
```

## License

MIT, See the [LICENSE] file for details.