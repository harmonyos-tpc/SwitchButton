/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.suke.widget.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 工具类，用于获取自定义属性的对应值
 */
public class AttrUtils {
    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0x20221, "AttrUtils"); ;

    /**
     * 获取属性对应的int值
     * @param attrs attrs
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 返回属性对相应的int值
     */
    public static int getIntFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }
    /**
     * 获取属性对应的float值
     * @param attrs attrs
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 返回属性对相应的float值
     */
    public static float getFloatFromAttr(AttrSet attrs, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }

    /**
     * 获取属性对应的布尔值
     * @param attrs attrs
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return true or false
     */
    public static boolean getBooleanFromAttr(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }

    /**
     * 获取属性对应的颜色值
     * @param attrs attrs
     * @param name 属性名称
     * @param defaultValue 属性默认颜色值
     * @return 返回属性对应的颜色值
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }

    /**
     * 获取属性对应的Element对象值
     * @param attrs attrs
     * @param name 属性名称
     * @param defaultValue 属性默认值
     * @return 返回属性Element
     */
    public static Element getElementFromAttr(AttrSet attrs, String name, Element defaultValue) {
        Element value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getElement();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }

    /**
     * 获取字符串属性对应的int值
     * @param attrs attr
     * @param name 属性名称
     * @param defaultValue 属性默认值
     * @return 返回对应的int值
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }

    /**
     * 从attr获取字符串
     * @param attrs attrs
     * @param name  属性字符串
     * @param defaultValue 默认值
     * @return 返回对应的字符串
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            HiLog.info(LOG_LABEL, e.getMessage());
        }
        return value;
    }
}
