/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.suke.widget.attrs;

public class SwitchAttrSet {

    /**
     * 阴影效果
     */
    public static final String SHADOW_EFFECT = "shadow_effect";

    /**
     * 开关关闭状态圆圈标识颜色
     */
    public static final String UNCHECK_CICLE_COLOR = "uncheck_circle_color";

    /**
     * 关闭状态圆圈标识宽度
     */
    public static final String UNCHECK_CICLE_WIDTH = "uncheck_circle_width";

    /**
     * 关闭状态圆圈标识半径
     */
    public static final String UNCHECK_CICLE_RADIUS = "uncheck_circle_radius";

    /**
     * 阴影半径
     */
    public static final String SHADOW_RADIUS = "shadow_radius";

    /**
     * 阴影偏移位置
     */
    public static final String SHADOW_OFFSET = "shadow_offset";
    /**
     * 阴影颜色
     */
    public static final String SHADOW_COLOR = "shadow_color";

    /**
     * 关闭状态区域颜色
     */
    public static final String UNCHECK_COLOR = "uncheck_color";

    /**
     * 打开状态区域颜色
     */
    public static final String CHECK_COLOR = "check_color";

    /**
     * 边框宽度
     */
    public static final String BORDER_WIDTH = "border_width";

    /**
     * 打开状态线条标识颜色
     */
    public static final String CHECK_LINE_COLOR = "check_line_color";

    /**
     * 打开状态线条宽度
     */
    public static final String CHECK_LINE_WIDTH = "check_line_width";

    /**
     * 默认开关滑块颜色
     */
    public static final String BUTTON_COLOR = "button_color";

    /**
     * 关闭状态滑块颜色
     */
    public static final String UNCHECK_BUTTON_COLOR = "uncheck_button_color";

    /**
     * 打开状态滑块颜色
     */
    public static final String CHECKED_BUTTON_COLOR = "checked_button_color";

    /**
     * 动画时间
     */
    public static final String EFFECT_DURATION = "effect_duration";

    /**
     * 开关状态设置
     */
    public static final String CHECKED = "checked";

    /**
     * 标识显示设置
     */
    public static final String SHOW_INDICATOR = "show_indicator";

    /**
     * 动画开关设置
     */
    public static final String ENABLE_EFFECT = "enable_effect";

    /**
     * 开关背景颜色
     */
    public static final String BACKGROUND = "background";
}
